package com.tgl.homework;

public class java_03_powerOf2 {

	public static void main(String[] args) {
		power(8);
	}
	
	static void power(int number) {
		int i = 1;
		do {
			System.out.println((int)Math.pow(2, i));
			i += 1;
		}while(i <= number);
	}

}
