package com.tgl.homework;

import java.util.ArrayList;

public class java_08_duplicates {

	public static void main(String[] args) {
		char[] letters = { 'a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e', 'd', 'f', 'g' };

		System.out.println(pack(letters));
	}

	public static ArrayList<String> pack(char[] letters) {
		String str;
		ArrayList<String> strArray = new ArrayList<String>();
		ArrayList<Character> Char = new ArrayList<Character>();

		for (int i = 0; i < letters.length - 1; i++) {
			Char.add(letters[i]);
			if (!(letters[i] == letters[i + 1] && i != letters.length - 2)) {
				if (i == letters.length - 2 && letters[i] == letters[i + 1]) {
					Char.add(letters[i]);
				}
				str = String.valueOf(Char);
				Char.clear();
				strArray.add(str);
				str = "";
			}

			if (i == letters.length - 2 && letters[i] != letters[i + 1]) {
				Char.clear();
				Char.add(letters[i+1]);
				str = String.valueOf(Char);
				Char.clear();
				strArray.add(str);
				str = "";
				break;
			}
		}
		return strArray;
	}
}
