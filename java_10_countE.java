package com.tgl.homework;

public class java_10_countE {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		System.out.println(countEs(s));

	}
	public static int countEs(String str) {
		int count = 0;
		int index = 0;
		while((index = str.indexOf("e",index)) != -1) {
			index++;
			count++;
		}
		return count;
	}
}
