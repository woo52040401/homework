package com.tgl.homework;

public class java_17_subStitutionCipher {

	public static void main(String[] args) {
		String s = "Hello World";

		System.out.println(encode(s));
	}

	public static String encode(String str) {
		String toLower = str.toLowerCase();
		StringBuilder newString = new StringBuilder();
		for (int i = 0; i < toLower.length(); i++) {
			char Char = toLower.charAt(i);
			if ((int) Char - 96 == -64) {
				newString.deleteCharAt(newString.length() - 1);
				newString.append(Char);
			} else{
				newString.append((int) Char - 96);
				if (i < 10) {
					newString.append(",");
				}
			}
		}
		return newString.toString();
	}

}
//a : 97