package com.tgl.homework;

public class java_02_squareNumber {

	public static void main(String[] args) {
		squares(10);
	}
	
	static void squares(int number) {
		int i = 1;
		while(i <= number) {
			System.out.println((int)Math.pow(i,2));
			i += 1;
		}
	}

}
