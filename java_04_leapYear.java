package com.tgl.homework;
import java.util.Scanner;

public class java_04_leapYear {

	public static void main(String[] args) {
		Scanner scanner= new Scanner(System.in);
		int year = scanner.nextInt();
		checkLeap(year);
	}
	
	public static void checkLeap(int year) {
		if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
			System.out.println(year + " is a leap year!!");
		}else {
			System.out.println(year + " is not leap year!!");
		}
	}
}