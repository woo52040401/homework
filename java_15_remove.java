package com.tgl.homework;

public class java_15_remove {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";

		System.out.println(star(s));
	}
	
	public static String star(String str) {
		String toLowerString = str.toLowerCase();
		String newString = toLowerString.replaceAll("[aeiou]", "*");
		return newString;
	}

}
