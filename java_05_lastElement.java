package com.tgl.homework;

public class java_05_lastElement {

	public static void main(String[] args) {
		String[] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		String lastElement = lastElement(breakfast);
		System.out.println(lastElement);
	}
	
	public static String lastElement(String[] breakfast) {
		String lastString = breakfast[breakfast.length - 1];
		return lastString;
	}
}
