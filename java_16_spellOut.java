package com.tgl.homework;

public class java_16_spellOut {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";

		System.out.println(spellout(s));

	}

	public static StringBuilder spellout(String str) {
		String toUpper = str.toUpperCase();
		StringBuilder newString = new StringBuilder(toUpper);
//		for (int i = 0; i < newString.length()-1; i++) {
//			
//			if (Character.isLetter(newString.charAt(i)) && Character.isLetter(newString.charAt(i + 1))) {
//				newString.insert(i+1, "-");
//			}
//		}
		int i = 0;
		while (i < newString.length() - 1) {
			if (Character.isLetter(newString.charAt(i)) && Character.isLetter(newString.charAt(i + 1))) {
				newString.insert(i+1, "-");
			}
			i++;
		}
		return newString;
	}

}
