package com.tgl.homework;

public class java_09_countWord {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		System.out.println(countWords(s));
	}
	public static int countWords(String str) {
		String[] arrays;
		int count = 0;
		arrays = str.split("\\s+");
		for(String array: arrays) {
			count++;
		}
		return count;
	}

}
