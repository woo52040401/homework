package com.tgl.homework;
import java.util.ArrayList;

public class java_07_palindromicArray {

	public static void main(String[] args) {
		String [] palindromic = {"Apple","Sausage", "Eggs", "Beans", "Beans", "Eggs", "Sausage","ApplE"};
		String [] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		
		System.out.println(isPalindrome(palindromic));
		System.out.println(isPalindrome(breakfast));
			
	}
	
	public static Boolean isPalindrome(String[] str) {
		boolean answer = true;
		for(int i = 0; i < str.length / 2; i++) {
			answer = str[i].equals(str[(str.length-1) - i]);
		}
		return answer;
	}

}
