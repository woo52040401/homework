package com.tgl.homework;

public class java_11_countAlpha {

	public static void main(String[] args) {
		String s = "1984 by George Orwell.";
		
		System.out.println(countChars(s));
	}
	
	public static int countChars(String str) {
		String newString = str.replaceAll("[^A-Za-z0-9]", "");
		return newString.length();
	}

}
