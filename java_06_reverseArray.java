package com.tgl.homework;

import java.util.ArrayList;

public class java_06_reverseArray {

	public static void main(String[] args) {
		String[] breakfasts = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		
		ArrayList<String> returnArray = reverse(breakfasts);
		System.out.println(returnArray);
	}
	
	public static ArrayList<String> reverse(String[] breakfasts) {
		java.util.ArrayList<String> reverseArray = new java.util.ArrayList<String>();
		for(int i = breakfasts.length - 1; i >= 0; i--) {
			reverseArray.add(breakfasts[i]);
		}
		return reverseArray;
	}
	
	

}
