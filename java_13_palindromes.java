package com.tgl.homework;

public class java_13_palindromes {

	public static void main(String[] args) {
		String sentence = "I never saw a purple cow";
		String palindrome = "rotavator";
		
		System.out.println(isPalindrome(sentence));
		System.out.println(isPalindrome(palindrome));
	}
	
	public static Boolean isPalindrome(String str) {
		boolean answer = true;
		char[] charArray = str.toCharArray();
		System.out.println(charArray);
		for(int i = 0; i < str.length() / 2; i++) {
			answer = (charArray[i] == charArray[(str.length()-1) - i]);
		}
		return answer;
	}

}
