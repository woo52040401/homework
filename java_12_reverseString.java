package com.tgl.homework;

public class java_12_reverseString {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		System.out.println(reverse(s));
	}
	public static StringBuilder reverse(String str) {
		char[] newChar= str.toCharArray();
		StringBuilder newString = new StringBuilder();
		for(int i = str.length()-1; i >= 0;i--) {
			newString.append(newChar[i]);
		}
		return newString;
	}

}
