package com.tgl.homework;

public class java_14_palindromes2 {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		String p = "Rise to vote, Sir!";
		
		System.out.println(isPalindromes(s));
		System.out.println(isPalindromes(p));
	}
	
	public static boolean isPalindromes(String str) {
		String toLowerString = str.toLowerCase();
		boolean answer = true;
		String newString = toLowerString.replaceAll("[^a-z0-9]", "");
		for(int i = 0; i < newString.length() / 2; i++) {
			 answer = (newString.charAt(i) == newString.charAt(newString.length()-1-i));
		}
		return answer;
	}
}
